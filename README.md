# README #

All the check points are done.

### What is this repository for? ###

* To list transaction which user has done.
* All transactions are sorted based on date.
* Sum of remaining amount is shown in the end.
* User can refresh transactions by pulling list from top.
* 'Some went wrong' will appear if there is some network issue while fetching transactions.
* User can pull from top to to fetch transactions again in case of something went wrong.
* Application support both landscape and portrait mode.

### How do I get set up? ###

* Android Studio is required to run this project.
* Dependencies used in application - RxJava, Dagger, Spek, retrofit, gson, retrofit adapter, okhttp,
room(Database) and mockito.
* How to run tests - To run test case you need spek plugin in android studio. Then click on play
button along with class name in file.

* Debug apk is attached

* Submitted by Sourabh Rustagi on 25 july 2018 at 01:00am