package com.example.sourusta.checkbalance.transactions

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.sourusta.checkbalance.R
import com.example.sourusta.checkbalance.data.Transaction
import com.example.sourusta.checkbalance.utils.DateUtils

/**
 * Display a list of [Transaction]s. User can choose to view transaction detail
 */
class TransactionsAdapter(transactions: List<Transaction>,
                          private val transactionItemListener: TransactionItemListener)
    : RecyclerView.Adapter<TransactionsAdapter.TransactionViewHolder>() {
    var transactions: List<Transaction> = transactions
        set(transactions) {
            field = transactions
            notifyDataSetChanged()
        }

    override fun onBindViewHolder(holder: TransactionViewHolder, position: Int) {
        val transaction = transactions[position]
        holder.idTxtVw.text = transaction.id.toString()
        holder.descTxtVw.text = transaction.description
        holder.amountTxtVw.text = transaction.amount.toString()
        holder.effectiveDateTxtVw.text = DateUtils.getFormattedDate(transaction.effectiveDate)
        holder.itemView.setOnClickListener {
            transactionItemListener.onTransactionClick(transaction)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TransactionViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_transaction, parent, false)
        return TransactionViewHolder(view)
    }

    override fun getItemCount(): Int = transactions.size

    inner class TransactionViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        var idTxtVw: TextView = view.findViewById(R.id.txt_vw_id)
        var descTxtVw: TextView = view.findViewById(R.id.txt_vw_desc)
        var amountTxtVw: TextView = view.findViewById(R.id.txt_vw_amount)
        var effectiveDateTxtVw: TextView = view.findViewById(R.id.txt_vw_effective_date)
    }
}