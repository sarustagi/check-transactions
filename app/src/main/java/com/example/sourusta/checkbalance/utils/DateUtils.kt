package com.example.sourusta.checkbalance.utils

import java.text.SimpleDateFormat
import java.util.*

/**
 * Date time utils methods
 */
object DateUtils {
    private const val SERVER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    private const val USER_DATE_FORMAT = "dd MMM yyyy 'at' hh:mm aa"

    fun getFormattedDate(dateStr: String): String {
        val formatter = SimpleDateFormat(USER_DATE_FORMAT, Locale.US)
        return formatter.format(convertServerDateToDate(dateStr))
    }

    fun convertServerDateToDate(dateStr: String): Date {
        val formatter = SimpleDateFormat(SERVER_DATE_FORMAT, Locale.US)
        return formatter.parse(dateStr)
    }
}