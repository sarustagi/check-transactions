package com.example.sourusta.checkbalance.data.source

import com.example.sourusta.checkbalance.data.Transaction
import io.reactivex.Flowable

/**
 * Main entry point for accessing transactions data.
 */
interface TransactionsDataSource {
    fun getTransactions(): Flowable<List<Transaction>>

    fun refreshTransactions()

    fun deleteAllTransactions()

    fun saveTransaction(transaction: Transaction)
}