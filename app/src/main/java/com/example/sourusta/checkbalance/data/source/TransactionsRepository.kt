package com.example.sourusta.checkbalance.data.source

import com.example.sourusta.checkbalance.data.Transaction
import io.reactivex.Flowable
import javax.inject.Singleton

/**
 * Implementation to load transactions from the data source into a db cache.
 */
@Singleton
class TransactionsRepository(private val mTransactionsRemoteDataSource: TransactionsDataSource,
                             private val mTransactionsLocalDataSource: TransactionsDataSource)
    : TransactionsDataSource {
    /**
     * Marks the cache as invalid, to force an update the next time data is requested. This variable
     * has package local visibility so it can be accessed.
     */
    private var mCacheIsDirty = false

    /**
     * This variable hold cache of transactions
     */
    private var mCachedTransactions: LinkedHashMap<String, Transaction> = LinkedHashMap()

    /**
     * Gets transactions from cache, local data source or api, whichever is available first.
     */
    override fun getTransactions(): Flowable<List<Transaction>> {
        //Respond immediately with cache if available and not dirty
        if (mCachedTransactions.isNotEmpty() && !mCacheIsDirty) {
            return Flowable.fromIterable(mCachedTransactions.values).toList().toFlowable()
        }
        val remoteTransactions = getAndSaveRemoteTransactions()
        return if (mCacheIsDirty) {
            //if the cache is dirty we need to fetch new data from network.
            remoteTransactions
        } else {
            // Query the local storage if available. If not, query the network.
            val localTransactions = getAndCacheLocalTransactions()
            Flowable.concat<List<Transaction>>(localTransactions, remoteTransactions)
                    .filter { transactions -> !transactions.isEmpty() }
                    .firstOrError()
                    .toFlowable()
        }
    }

    /**
     * Gets transactions from local and store transactions into local cache.
     */
    private fun getAndCacheLocalTransactions(): Flowable<List<Transaction>> {
        return mTransactionsLocalDataSource.getTransactions()
                .flatMap { transactions ->
                    Flowable.fromIterable(transactions)
                            .doOnNext { transaction ->
                                mCachedTransactions[transaction.id.toString()] = transaction
                            }
                            .toList()
                            .toFlowable()
                }
    }

    /**
     * Save transactions which came from api in db and local cache
     */
    private fun getAndSaveRemoteTransactions(): Flowable<List<Transaction>> {
        return mTransactionsRemoteDataSource
                .getTransactions()
                .flatMap { transactions ->
                    Flowable.fromIterable(transactions).doOnNext { transaction ->
                        mTransactionsLocalDataSource.saveTransaction(transaction)
                        mCachedTransactions[transaction.id.toString()] = transaction
                    }.toList().toFlowable()
                }
                .doOnComplete { mCacheIsDirty = false }
    }

    override fun refreshTransactions() {
        mCacheIsDirty = true
    }

    /**
     * Deletes all transaction data from local cache, db and on server
     */
    override fun deleteAllTransactions() {
        mTransactionsRemoteDataSource.deleteAllTransactions()
        mTransactionsLocalDataSource.deleteAllTransactions()
        mCachedTransactions.clear()
    }

    override fun saveTransaction(transaction: Transaction) {
        cacheAndPerform(transaction) {
            mTransactionsRemoteDataSource.saveTransaction(it)
            mTransactionsLocalDataSource.saveTransaction(it)
        }
    }

    private inline fun cacheAndPerform(transaction: Transaction, perform: (Transaction) -> Unit) {
        val cachedTransaction = Transaction(transaction.amount,
                transaction.description,
                transaction.id,
                transaction.effectiveDate)
        mCachedTransactions[cachedTransaction.id.toString()] = cachedTransaction
        perform(cachedTransaction)
    }
}