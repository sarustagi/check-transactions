package com.example.sourusta.checkbalance.data.source.remote

import com.example.sourusta.checkbalance.data.Transaction
import io.reactivex.Flowable
import retrofit2.http.GET

/**
 * Interface to access api
 */
interface RemoteService {
    @GET("transactions")
    fun getTransactions(): Flowable<List<Transaction>>
}