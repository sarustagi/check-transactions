package com.example.sourusta.checkbalance.transactions

import com.example.sourusta.checkbalance.BasePresenter
import com.example.sourusta.checkbalance.BaseView
import com.example.sourusta.checkbalance.data.Transaction

/**
 * This specifies the contract between the view and the presenter.
 */
interface TransactionsContract {
    interface View : BaseView<Presenter> {
        fun setLoadingIndicator(active: Boolean)

        fun showNoTransactions()

        fun showTransactions(transactions: List<Transaction>)

        fun showSomeThingWentWrong()

        fun isActive(): Boolean
    }

    interface Presenter : BasePresenter {
        fun loadTransactions(forceUpdate: Boolean)
    }
}