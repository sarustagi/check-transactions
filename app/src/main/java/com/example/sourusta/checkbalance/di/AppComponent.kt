package com.example.sourusta.checkbalance.di

import com.example.sourusta.checkbalance.transactions.TransactionsActivity
import com.example.sourusta.checkbalance.transactions.TransactionsModule
import dagger.Component
import javax.inject.Singleton

/**
 * Dagger component class contains list of modules used in this application.
 *
 * Over here with the help of inject fun, singleton objects are injected which class has demanded
 * at run time.
 */
@Singleton
@Component(modules = [(TransactionsModule::class)])
interface AppComponent {
    fun inject(transactionsActivity: TransactionsActivity)
}