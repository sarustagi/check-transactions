package com.example.sourusta.checkbalance.transactions

import com.example.sourusta.checkbalance.data.source.TransactionsRepository
import com.example.sourusta.checkbalance.utils.schedulers.BaseSchedulerProvider
import io.reactivex.annotations.NonNull
import io.reactivex.disposables.CompositeDisposable

/**
 * Listens to user actions from UI([TransactionsFragment]), retrieves the data and update the UI as
 * required.
 */
class TransactionsPresenter(private val mTransactionsRepository: TransactionsRepository,
                            private val mView: TransactionsContract.View,
                            @NonNull schedulerProvider: BaseSchedulerProvider) :
        TransactionsContract.Presenter {
    private var mFirstLoad = true

    @NonNull
    private var mCompositeDisposable: CompositeDisposable
    @NonNull
    private var mSchedulerProvider: BaseSchedulerProvider

    init {
        mView.mPresenter = this
        mCompositeDisposable = CompositeDisposable()
        mSchedulerProvider = schedulerProvider
    }

    override fun loadTransactions(forceUpdate: Boolean) {
        loadTransactions(forceUpdate || mFirstLoad, true)
        mFirstLoad = false
    }

    /**
     * @param forceUpdate Pass in true to refresh the data in the Data source
     * @param showLoadingUI Pass in true to display a loading icon in the UI
     */
    private fun loadTransactions(forceUpdate: Boolean, showLoadingUI: Boolean) {
        if (showLoadingUI) {
            mView.setLoadingIndicator(true)
        }
        if (forceUpdate) {
            mTransactionsRepository.refreshTransactions()
        }
        mCompositeDisposable.clear()
        val disposable = mTransactionsRepository
                .getTransactions()
                .toList()
                .subscribeOn(mSchedulerProvider.io())
                .observeOn(mSchedulerProvider.ui())
                .subscribe(
                        // onNext
                        { transactions ->
                            if (showLoadingUI) {
                                mView.setLoadingIndicator(false)
                            }
                            if (transactions.isEmpty()) {
                                mView.showNoTransactions()
                            } else {
                                mView.showTransactions(transactions[0])
                            }
                        },
                        { _ ->
                            if (showLoadingUI) {
                                mView.setLoadingIndicator(false)
                            }
                            mView.showSomeThingWentWrong()
                        })
        mCompositeDisposable.add(disposable)
    }

    override fun subscribe() {
        loadTransactions(false)
    }

    override fun unsubscribe() {
        mCompositeDisposable.clear()
    }
}