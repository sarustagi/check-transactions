package com.example.sourusta.checkbalance.data.source.local

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.example.sourusta.checkbalance.data.Transaction

/**
 * Data Access Object for the transactions table.
 */
@Dao
interface TransactionsDao {
    /**
     * Select all transactions from the transactions table.
     *
     * @return all transactions
     */
    @Query("SELECT * from Transactions")
    fun getTransactions(): List<Transaction>

    /**
     * Insert a transaction in the database. If the transactions already exists, replace it
     *
     * @param transaction the transactions to be inserted
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTransaction(transaction: Transaction)

    /**
     * Delete all transactions.
     */
    @Query("Delete from Transactions")
    fun deleteTransactions()
}