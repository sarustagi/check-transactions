package com.example.sourusta.checkbalance.utils

import android.app.Activity
import android.support.v7.app.AlertDialog
import com.example.sourusta.checkbalance.R

/**
 * Dialog util class
 */
object DialogUtils {
    fun showDialog(activity: Activity, title: String, message: String) {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(title)
        builder.setMessage(message)
                .setPositiveButton(R.string.ok) { dialog, _ ->
                    dialog.dismiss()
                }
        builder.create().show()
    }
}