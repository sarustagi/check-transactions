package com.example.sourusta.checkbalance.utils

import java.util.concurrent.Executor

/**
 * Global executor pools for the whole application.
 *
 * Grouping transactions like this avoids the effects of transactions starvation (e.g. disk reads
 * don't wait behind webservice requests.
 */
open class AppExecutors(
        val diskIO: Executor = DiskIOThreadExecutor()
)