package com.example.sourusta.checkbalance.transactions

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.sourusta.checkbalance.R
import com.example.sourusta.checkbalance.data.Transaction
import com.example.sourusta.checkbalance.utils.DateUtils
import com.example.sourusta.checkbalance.utils.DialogUtils
import java.util.*

/**
 * Display a list of transactions. User can choose particular transactions and check more detail.
 */
class TransactionsFragment : Fragment(), TransactionsContract.View {
    override lateinit var mPresenter: TransactionsContract.Presenter
    private lateinit var mNoTransactionsNoticeTxtVw: TextView
    private lateinit var mTotalTransactionsTxtVw: TextView
    private lateinit var mTransactionAmountTxtVw: TextView
    private lateinit var mIdTxtVw: TextView
    private lateinit var mDescTxtVw: TextView
    private lateinit var mAmountTxtVw: TextView
    private lateinit var mSeparatorVw: View
    private lateinit var mTransactionsList: RecyclerView

    /**
     * Listener for clicks on transactions in the recyclerview.
     */
    private var mTransactionItemListener: TransactionItemListener =
            object : TransactionItemListener {
                override fun onTransactionClick(clickedTransaction: Transaction) {
                    DialogUtils.showDialog(activity!!, clickedTransaction.description,
                            getString(R.string.transaction_detail,
                                    clickedTransaction.amount.toString(),
                                    DateUtils.getFormattedDate(clickedTransaction.effectiveDate)))
                }
            }
    private val mTransactionAdapter = TransactionsAdapter(ArrayList(0),
            mTransactionItemListener)

    companion object {
        fun newInstance() = TransactionsFragment()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val root = inflater.inflate(R.layout.fragment_transactions, container, false)
        //Set up view
        with(root) {
            mTransactionsList = findViewById<RecyclerView>(R.id.recycler_view).apply {
                layoutManager = LinearLayoutManager(context)
                adapter = mTransactionAdapter
            }
            mNoTransactionsNoticeTxtVw = findViewById(R.id.txt_vw_no_transaction_notice)
            mTotalTransactionsTxtVw = findViewById(R.id.txt_vw_total_transactions)
            mTransactionAmountTxtVw = findViewById(R.id.txt_vw_transactions_amount)
            mIdTxtVw = findViewById(R.id.txt_vw_id)
            mDescTxtVw = findViewById(R.id.txt_vw_desc)
            mAmountTxtVw = findViewById(R.id.txt_vw_amount)
            mSeparatorVw = findViewById(R.id.view_separator)

            //set up progress indicator
            findViewById<ScrollChildSwipeRefreshLayout>(R.id.refresh_layout).apply {
                setColorSchemeColors(
                        ContextCompat.getColor(requireContext(), R.color.colorPrimary),
                        ContextCompat.getColor(requireContext(), R.color.colorAccent),
                        ContextCompat.getColor(requireContext(), R.color.colorPrimaryDark)
                )
                //Set the scrolling view in the custom SwipeRefreshLayout.
                scrollUpChild = mTransactionsList
                setOnRefreshListener { mPresenter.loadTransactions(false) }
            }
        }
        return root
    }

    override fun onResume() {
        super.onResume()
        mPresenter.subscribe()
        hideViewsBeforeLoading()
    }

    override fun onPause() {
        super.onPause()
        mPresenter.unsubscribe()
    }

    private fun hideViewsBeforeLoading() {
        mTotalTransactionsTxtVw.visibility = View.INVISIBLE
        mTransactionAmountTxtVw.visibility = View.INVISIBLE
        mIdTxtVw.visibility = View.INVISIBLE
        mDescTxtVw.visibility = View.INVISIBLE
        mAmountTxtVw.visibility = View.INVISIBLE
        mSeparatorVw.visibility = View.INVISIBLE
        mTransactionsList.visibility = View.INVISIBLE
    }

    override fun setLoadingIndicator(active: Boolean) {
        val root = view ?: return
        root.findViewById<SwipeRefreshLayout>(R.id.refresh_layout).isRefreshing = active
        if (active) {
            mNoTransactionsNoticeTxtVw.visibility = View.GONE
        }
    }

    override fun showNoTransactions() {
        mNoTransactionsNoticeTxtVw.text = getString(R.string.no_transactions_found)
        showErrorView()
    }

    override fun showSomeThingWentWrong() {
        mNoTransactionsNoticeTxtVw.text = getString(R.string.some_thing_went_wrong)
        showErrorView()
    }

    private fun showErrorView() {
        mNoTransactionsNoticeTxtVw.visibility = View.VISIBLE
        mTotalTransactionsTxtVw.visibility = View.GONE
        mTransactionAmountTxtVw.visibility = View.GONE
        mIdTxtVw.visibility = View.GONE
        mDescTxtVw.visibility = View.GONE
        mAmountTxtVw.visibility = View.GONE
        mSeparatorVw.visibility = View.GONE
        mTransactionsList.visibility = View.GONE
    }

    override fun showTransactions(transactions: List<Transaction>) {
        val sortedTransactions = sortTransactionsBasedOnDate(transactions)
        mTransactionAdapter.transactions = sortedTransactions
        mNoTransactionsNoticeTxtVw.visibility = View.GONE

        mTotalTransactionsTxtVw.visibility = View.VISIBLE
        mTransactionAmountTxtVw.visibility = View.VISIBLE
        mIdTxtVw.visibility = View.VISIBLE
        mDescTxtVw.visibility = View.VISIBLE
        mAmountTxtVw.visibility = View.VISIBLE
        mSeparatorVw.visibility = View.VISIBLE
        mTransactionsList.visibility = View.VISIBLE

        mTransactionAmountTxtVw.text = getString(R.string.amount_with_currency_symbol,
                getAmountTotal(sortedTransactions).toString())
    }

    private fun sortTransactionsBasedOnDate(transactions: List<Transaction>): List<Transaction> {
        return transactions.sortedWith(compareBy(Transaction::getDate,
                Transaction::getDate))
    }

    private fun getAmountTotal(transactions: List<Transaction>): Double {
        var amount = 0.0
        for (transaction in transactions) amount += transaction.amount
        return Math.round(amount * 100.0) / 100.0
    }

    override fun isActive(): Boolean {
        return isAdded
    }
}