package com.example.sourusta.checkbalance.data.source.local

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.example.sourusta.checkbalance.data.Transaction

/**
 * The Room database that contains the transaction table.
 */
@Database(entities = [(Transaction::class)], version = 1, exportSchema = false)
abstract class TransactionsDatabase : RoomDatabase() {
    abstract fun transactionDao(): TransactionsDao
}