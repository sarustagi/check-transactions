package com.example.sourusta.checkbalance.transactions

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.sourusta.checkbalance.CheckBalanceApplication
import com.example.sourusta.checkbalance.R
import com.example.sourusta.checkbalance.data.source.TransactionsRepository
import com.example.sourusta.checkbalance.utils.replaceFragmentInActivity
import com.example.sourusta.checkbalance.utils.schedulers.BaseSchedulerProvider
import javax.inject.Inject

class TransactionsActivity : AppCompatActivity() {
    private lateinit var transactionsPresenter: TransactionsPresenter

    @Inject
    lateinit var mTransactionsRepository: TransactionsRepository

    @Inject
    lateinit var mBaseSchedulerProvider: BaseSchedulerProvider

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_transactions)
        (application as CheckBalanceApplication).getAppComponent().inject(this)
        // Set up the toolbar.
        setSupportActionBar(findViewById(R.id.toolBar))

        val transactionsFragment = supportFragmentManager.findFragmentById(R.id.contentFrame)
                as TransactionsFragment? ?: TransactionsFragment.newInstance().also {
            replaceFragmentInActivity(it, R.id.contentFrame)
        }

        transactionsPresenter = TransactionsPresenter(mTransactionsRepository,
                transactionsFragment,
                mBaseSchedulerProvider).apply { }
    }
}
