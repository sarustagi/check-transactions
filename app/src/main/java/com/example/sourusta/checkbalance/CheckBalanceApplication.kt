package com.example.sourusta.checkbalance

import android.app.Application
import com.example.sourusta.checkbalance.di.AppComponent
import com.example.sourusta.checkbalance.di.DaggerAppComponent
import com.example.sourusta.checkbalance.transactions.TransactionsModule

/**
 * We created custom application class to handle dagger.
 */
class CheckBalanceApplication : Application() {
    private lateinit var mAppComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        mAppComponent = DaggerAppComponent
                .builder()
                .transactionsModule(TransactionsModule(this))
                .build()
    }

    fun getAppComponent(): AppComponent {
        return mAppComponent
    }
}