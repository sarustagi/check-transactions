package com.example.sourusta.checkbalance.data

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import com.example.sourusta.checkbalance.utils.DateUtils.convertServerDateToDate
import java.util.*

/**
 * Model class for a transaction.
 *
 * @param amount amount of the transaction
 * @param description description of transaction
 * @param id unique id of transaction
 * @param effectiveDate date and time on which transaction was committed.
 */
@Entity(tableName = "Transactions")
data class Transaction constructor(@ColumnInfo(name = "amount") var amount: Double,
                                   @ColumnInfo(name = "description") var description: String,
                                   @PrimaryKey @ColumnInfo(name = "id") var id: Int,
                                   @ColumnInfo(name = "effectiveDate") var effectiveDate: String) {

    /**
     * Gets date in java date class
     */
    fun getDate(): Date {
        return convertServerDateToDate(effectiveDate)
    }
}