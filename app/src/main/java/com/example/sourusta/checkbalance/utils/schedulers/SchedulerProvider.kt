package com.example.sourusta.checkbalance.utils.schedulers

import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Singleton

/**
 * Provides different types of schedulers.
 */
@Singleton
class SchedulerProvider : BaseSchedulerProvider {
    companion object {
        private var INSTANCE: SchedulerProvider? = null

        @JvmStatic
        fun getInstance(): SchedulerProvider {
            if (INSTANCE == null) {
                synchronized(SchedulerProvider::javaClass) {
                    INSTANCE = SchedulerProvider()
                }
            }
            return INSTANCE!!
        }
    }

    override fun computation(): Scheduler {
        return Schedulers.computation()
    }

    override fun io(): Scheduler {
        return Schedulers.io()
    }

    override fun ui(): Scheduler {
        return AndroidSchedulers.mainThread()
    }
}