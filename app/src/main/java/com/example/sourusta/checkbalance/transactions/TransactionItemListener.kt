package com.example.sourusta.checkbalance.transactions

import com.example.sourusta.checkbalance.data.Transaction

/**
 * Interface to define callback on transaction click in list.
 */
interface TransactionItemListener {
    fun onTransactionClick(clickedTransaction: Transaction)
}