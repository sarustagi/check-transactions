package com.example.sourusta.checkbalance.utils.schedulers

import io.reactivex.Scheduler
import io.reactivex.annotations.NonNull

/**
 * Allow providing different types of Schedulers
 */
interface BaseSchedulerProvider {
    @NonNull
    fun computation(): Scheduler

    @NonNull
    fun io(): Scheduler

    @NonNull
    fun ui(): Scheduler
}