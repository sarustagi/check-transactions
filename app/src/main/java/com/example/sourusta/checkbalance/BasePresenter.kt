package com.example.sourusta.checkbalance

interface BasePresenter {
    /**
     * Binds presenter with a view when resumed.
     */
    fun subscribe()

    /**
     * Drops the reference to the view when destroyed.
     */
    fun unsubscribe()
}