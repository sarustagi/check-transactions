package com.example.sourusta.checkbalance.data.source.remote

import com.example.sourusta.checkbalance.data.Transaction
import com.example.sourusta.checkbalance.data.source.TransactionsDataSource
import com.example.sourusta.checkbalance.utils.schedulers.BaseSchedulerProvider
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Flowable
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Implementation of a remote data source
 */
class TransactionsRemoteDataSource constructor(private val baseSchedulerProvider: BaseSchedulerProvider)
    : TransactionsDataSource {
    companion object {
        private const val URL = "https://private-ddc1b2-transactions14.apiary-mock.com/"
        private const val READ_TIME_OUT = 5L
        private const val CONNECTION_TIME_OUT = 5L
    }

    /**
     * This will be called when there is no data in db or forcefully on start
     */
    override fun getTransactions(): Flowable<List<Transaction>> {
        val okHttpClient = OkHttpClient.Builder()
                .readTimeout(READ_TIME_OUT, TimeUnit.SECONDS)
                .connectTimeout(CONNECTION_TIME_OUT, TimeUnit.SECONDS)
                .build()
        //Create a very simple REST adapter which points the API
        val retrofit = Retrofit.Builder()
                .baseUrl(URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build()
        //Create an instance of our API interface.
        val remoteService = retrofit.create(RemoteService::class.java)
        // Fetch and print a list of the transactions
        return remoteService.getTransactions()
                .subscribeOn(baseSchedulerProvider.io())
                .observeOn(baseSchedulerProvider.ui())
                .map { transaction -> transaction }
    }

    override fun refreshTransactions() {
        //Empty because Transactions Repository class handles the logic of refreshing transactions
    }

    override fun deleteAllTransactions() {
        //Can call delete all transactions on server via api
    }

    override fun saveTransaction(transaction: Transaction) {
        //can call save transaction on server via api
    }
}