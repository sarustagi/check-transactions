package com.example.sourusta.checkbalance.transactions

import android.app.Application
import android.arch.persistence.room.Room
import com.example.sourusta.checkbalance.data.source.TransactionsRepository
import com.example.sourusta.checkbalance.data.source.local.TransactionsDatabase
import com.example.sourusta.checkbalance.data.source.local.TransactionsLocalDataSource
import com.example.sourusta.checkbalance.data.source.remote.TransactionsRemoteDataSource
import com.example.sourusta.checkbalance.utils.AppExecutors
import com.example.sourusta.checkbalance.utils.schedulers.BaseSchedulerProvider
import com.example.sourusta.checkbalance.utils.schedulers.SchedulerProvider
import dagger.Module
import dagger.Provides

/**
 * This a Dagger module. We use this to pass in activity dependency.
 */
@Module
class TransactionsModule constructor(application: Application) {
    private val mDb: TransactionsDatabase = Room.databaseBuilder(application,
            TransactionsDatabase::class.java,
            "Transactions.db")
            .build()
    private val mBaseSchedulerProvider = SchedulerProvider.getInstance()
    private val mTransactionsRemoteDataSource = TransactionsRemoteDataSource(mBaseSchedulerProvider)
    private val mTransactionsLocalDataSource = TransactionsLocalDataSource(AppExecutors(), mDb.transactionDao())

    @Provides
    fun getTransactionsRepository(): TransactionsRepository {
        return TransactionsRepository(mTransactionsRemoteDataSource, mTransactionsLocalDataSource)
    }

    @Provides
    fun provideSchedulerProvider(): BaseSchedulerProvider {
        return mBaseSchedulerProvider
    }
}