package com.example.sourusta.checkbalance.data.source.local

import com.example.sourusta.checkbalance.data.Transaction
import com.example.sourusta.checkbalance.data.source.TransactionsDataSource
import com.example.sourusta.checkbalance.utils.AppExecutors
import io.reactivex.Flowable

/**
 * Concrete implementation of a data source as a db.
 */
class TransactionsLocalDataSource (
        private val appExecutors: AppExecutors,
        private val transactionsDao: TransactionsDao
) : TransactionsDataSource {
    override fun getTransactions(): Flowable<List<Transaction>> {
        val transactions = transactionsDao.getTransactions()
        return Flowable.fromIterable(transactions).toList().toFlowable()
    }

    override fun refreshTransactions() {
        //Empty
    }

    override fun deleteAllTransactions() {
        appExecutors.diskIO.execute { transactionsDao.deleteTransactions() }
    }

    override fun saveTransaction(transaction: Transaction) {
        appExecutors.diskIO.execute { transactionsDao.insertTransaction(transaction) }
    }
}