package com.example.sourusta.checkbalance

import com.example.sourusta.checkbalance.data.Transaction
import com.example.sourusta.checkbalance.data.source.TransactionsRepository
import com.example.sourusta.checkbalance.transactions.TransactionsContract
import com.example.sourusta.checkbalance.transactions.TransactionsPresenter
import com.example.sourusta.checkbalance.utils.schedulers.BaseSchedulerProvider
import com.example.sourusta.checkbalance.utils.schedulers.ImmediateSchedulerProvider
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Flowable
import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.given
import org.jetbrains.spek.api.dsl.it
import org.jetbrains.spek.api.dsl.on

/**
 * Example local unit test, which will execute on the development machine (host).
 */
object TransactionsPresenterSpec : Spek({
    given("a TransactionsPresenterSpec") {
        // mock() has a very convenient way to inject mocks object
        val mTransactionsRepository: TransactionsRepository = mock()
        val mTransactionsView: TransactionsContract.View = mock()

        // Make the sure that all schedulers are immediate.
        val mSchedulerProvider: BaseSchedulerProvider = ImmediateSchedulerProvider()

        // Get a reference to the class under test
        val mTransactionsPresenter = TransactionsPresenter(mTransactionsRepository,
                        mTransactionsView,
                        mSchedulerProvider)

        val mTransactions = listOf(Transaction(1.0, "Description1", 1, "2018-01-01T01:01:01.000Z"),
                Transaction(2.0, "Description2", 2, "2018-02-02T02:02:02.111Z"),
                Transaction(3.0, "Description3", 3, "2017-03-03T03:03:03.222Z"))

        beforeGroup {
            // Then the presenter is set to the view
            whenever(mTransactionsView.isActive()).thenReturn(true)
        }

        on("load all transactions") {
            // Given an initialized TransactionsPresenter with initialized transactions
            whenever(mTransactionsRepository.getTransactions()).thenReturn(Flowable.just<List<Transaction>>(mTransactions))

            mTransactionsPresenter.loadTransactions(true)

            // Then progress indicator is shown
            it("showing loading indicator") {
                verify(mTransactionsView).setLoadingIndicator(true)
            }

            // Then progress indicator is hidden and all transactions are shown in UI
            it("hiding loading indicator") {
                verify(mTransactionsView).setLoadingIndicator(false)
            }
        }

        on("checking error loading transaction") {
            // Given that no transactions are available in the repository
            whenever(mTransactionsRepository.getTransactions()).thenReturn(Flowable.error<List<Transaction>>(Exception()))

            // When transactions are loaded
            mTransactionsPresenter.loadTransactions(true)
            // Then an error message is shown
            it("showing some thing went wrong") {
                verify(mTransactionsView).showSomeThingWentWrong()
            }
        }
    }
})