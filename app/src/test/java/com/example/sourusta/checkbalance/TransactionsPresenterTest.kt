package com.example.sourusta.checkbalance

import com.example.sourusta.checkbalance.data.Transaction
import com.example.sourusta.checkbalance.data.source.TransactionsRepository
import com.example.sourusta.checkbalance.transactions.TransactionsContract
import com.example.sourusta.checkbalance.transactions.TransactionsPresenter
import com.example.sourusta.checkbalance.utils.schedulers.BaseSchedulerProvider
import com.example.sourusta.checkbalance.utils.schedulers.ImmediateSchedulerProvider
import com.google.common.collect.Lists
import io.reactivex.Flowable
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.verify
import org.mockito.MockitoAnnotations

/**
 * Example local unit test, which will execute on the development machine (host).
 */
class TransactionsPresenterTest {
    private var mTransactions: List<Transaction> = Lists.newArrayList(
            Transaction(1.0, "Description1", 1, "2018-01-01T01:01:01.000Z"),
            Transaction(2.0, "Description2", 2, "2018-02-02T02:02:02.111Z"),
            Transaction(3.0, "Description3", 3, "2017-03-03T03:03:03.222Z"))

    @Mock
    private lateinit var mTransactionsRepository: TransactionsRepository

    @Mock
    private lateinit var mTransactionsView: TransactionsContract.View

    private lateinit var mSchedulerProvider: BaseSchedulerProvider

    private lateinit var mTransactionsPresenter: TransactionsPresenter

    @Before
    fun setupTransactionsPresenter() {
        // Mockito has a very convenient way to inject mocks by using the @Mock annotation. To
        // inject the mocks in the test the initMocks method needs to be called.
        MockitoAnnotations.initMocks(this)

        // Make the sure that all schedulers are immediate.
        mSchedulerProvider = ImmediateSchedulerProvider()

        // Get a reference to the class under test
        mTransactionsPresenter = TransactionsPresenter(mTransactionsRepository, mTransactionsView, mSchedulerProvider)

        // The presenter won't update the view unless it's active.
        `when`(mTransactionsView.isActive()).thenReturn(true)
    }

    @Test
    fun createPresenter_setsThePresenterToView() {
        // Get a reference to the class under test
        mTransactionsPresenter = TransactionsPresenter(mTransactionsRepository, mTransactionsView, mSchedulerProvider)

        // Then the presenter is set to the view
        verify(mTransactionsView)?.mPresenter = mTransactionsPresenter
    }

    @Test
    fun loadTransactionsFromRepositoryAndLoadIntoView() {
        // Given an initialized TransactionsPresenter with initialized transactions
        `when`(mTransactionsRepository.getTransactions()).thenReturn(Flowable.just<List<Transaction>>(mTransactions))
        // When loading of Transactions is requested
        mTransactionsPresenter.loadTransactions(true)

        // Then progress indicator is shown
        verify(mTransactionsView)?.setLoadingIndicator(true)
        // Then progress indicator is hidden and all transactions are shown in UI
        verify(mTransactionsView)?.setLoadingIndicator(false)
    }

    @Test
    fun errorLoadingTransactions_ShowsError() {
        // Given that no transactions are available in the repository
        `when`(mTransactionsRepository.getTransactions()).thenReturn(Flowable.error<List<Transaction>>(Exception()))

        // When transactions are loaded
        mTransactionsPresenter.loadTransactions(true)

        // Then an error message is shown
        verify<TransactionsContract.View>(mTransactionsView).showSomeThingWentWrong()
    }
}
